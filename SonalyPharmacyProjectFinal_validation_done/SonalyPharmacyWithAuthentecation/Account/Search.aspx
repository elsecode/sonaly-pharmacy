﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="SonalyPharmacyWithAuthentecation.Account.Search" Async="true" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <fieldset>
            <legend class="sidebar-font">Search/Update/Delete</legend>
            <div class="form-group">
                <label class="col-md-2 control-label">Product Category</label>
                <div class="col-md-4">
                    <asp:DropDownList CssClass="form-control" ID="categoryDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="categoryDropDownList_SelectedIndexChanged" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Product Name</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:DropDownList CssClass="form-control" ID="productDropDownList" runat="server" />
                        <div class="input-group-btn">
                            <asp:Button ID="productSearchButton" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="productSearchButton_OnClick" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Buying Price</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:TextBox ID="buyingPriceTextBox" runat="server" CssClass="form-control" />
                        <span class="input-group-addon">BDT</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Selling Price</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:TextBox ID="sellingPriceTextBox" runat="server" CssClass="form-control" />
                        <span class="input-group-addon">BDT</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Product Quantity</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:TextBox ID="productQuantityTextBox" runat="server" CssClass="form-control" />
                        <span class="input-group-addon">Packet/Piece</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Minimum Range of Product Quantity</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:TextBox ID="minimumQantityRangeTextBox" runat="server" CssClass="form-control" />
                        <span class="input-group-addon">Packet/Piece</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Product Placement</label>
                <div class="col-md-4">
                    <asp:TextBox ID="productPlacementTextBox" runat="server" CssClass="form-control form-control-width" />
                </div>
            </div>
            <hr />
            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <div class="alert alert-dismissable alert-success" style="margin-top: 1px;">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <asp:Label ID="messageLabel" runat="server">Fill up info and click add!</asp:Label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 col-md-offset-3">
                    <asp:Button ID="productUpdateButton" runat="server" Text="Update" CssClass="btn btn-warning" OnClick="productUpdateButton_OnClick" />
                    <asp:Button ID="productDeleteButton" runat="server" Text="Delete" CssClass="btn btn-danger pull-right" OnClick="productDeleteButton_OnClick" />
                </div>
            </div>
        </fieldset>
    </div>
</asp:Content>

﻿using System;
using System.Text.RegularExpressions;
using SonalyPharmacyWithAuthentecation.BLL;
using SonalyPharmacyWithAuthentecation.DAL.Dao;
using SonalyPharmacyWithAuthentecation.Utility;


namespace SonalyPharmacyWithAuthentecation.Account
{
    public partial class Product : System.Web.UI.Page
    {
        private int _productId;
        readonly ProductManager _productManager = new ProductManager();
        readonly CategoryManager _categoryManager = new CategoryManager();
        readonly Helper _helper = new Helper();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RefreshCategoryDropdownList();
            }

        }

        private void RefreshCategoryDropdownList()
        {
            categoryDropDownList.DataSource = _categoryManager.GetCategorySet();
            categoryDropDownList.DataTextField = "CategoryName";
            categoryDropDownList.DataValueField = "CategoryId";
            categoryDropDownList.DataBind();
        }

        protected void productSaveButton_Click(object sender, EventArgs e)
        {
            if (productNameTextBox.Text == string.Empty || buyingPriceTextBox.Text == string.Empty ||
                sellingPriceTextBox.Text == string.Empty || productQuantityTextBox.Text
                == string.Empty || minimumQantityRangeTextBox.Text == string.Empty ||
                productPlacementTextBox.Text == string.Empty)
            {
                messageLabel.Text = "Please fill out all the required fields";
            }
            else
            {
                if (!_helper.IsDecimal(buyingPriceTextBox.Text))
                {
                    messageLabel.Text = "Buying price must be a number";
                }
                else if (!_helper.IsDecimal(sellingPriceTextBox.Text))
                {
                    messageLabel.Text = "Selling price must be a number";
                }
                else if (!_helper.IsInteger(productQuantityTextBox.Text))
                {
                    messageLabel.Text = "Product quantity must be a number";
                }
                else if (!_helper.IsInteger(minimumQantityRangeTextBox.Text))
                {
                    messageLabel.Text = "Minimum quantity must be a number";
                }
                else
                {
                    try
                    {
                        var product = new ProductModel
                        {
                            ProductId = _productId,
                            ProductName = productNameTextBox.Text,
                            ProductQuantity = Convert.ToInt32(productQuantityTextBox.Text),
                            SellingPrice = Convert.ToDecimal(sellingPriceTextBox.Text),
                            BuyingPrice = Convert.ToDecimal(buyingPriceTextBox.Text),
                            MinimumQuantityRange = Convert.ToInt32(minimumQantityRangeTextBox.Text),
                            ProductPlacement = productPlacementTextBox.Text,
                            CategoryId = Convert.ToInt32(categoryDropDownList.SelectedValue)
                        };

                        if (_productManager.AddProduct(product))
                        {
                            messageLabel.Text = "Product Added Successfully";
                            productNameTextBox.Text = "";
                            productQuantityTextBox.Text = "";
                            sellingPriceTextBox.Text = "";
                            buyingPriceTextBox.Text = "";
                            minimumQantityRangeTextBox.Text = "";
                            productPlacementTextBox.Text = "";
                            RefreshCategoryDropdownList();

                        }
                        else { messageLabel.Text = "There some error in adding product"; }
                    }
                    catch (Exception exception)
                    {
                        messageLabel.Text = exception.Message;
                    }
                }
            }

        }
    }
}
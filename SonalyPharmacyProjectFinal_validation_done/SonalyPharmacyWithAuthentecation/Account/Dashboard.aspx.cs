﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SonalyPharmacyWithAuthentecation.BLL;
using SonalyPharmacyWithAuthentecation.Utility;

namespace SonalyPharmacyWithAuthentecation.Account
{
    public partial class Dashboard : System.Web.UI.Page
    {
        readonly TransactionManager _transactionManager = new TransactionManager();
        Helper _helper = new Helper();
        decimal _totalUnitPrice = 0m;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnPrint.Visible = false;
        }
        protected void transactionSearchButton_OnClick(object sender, EventArgs e)
        {
            if (fromDateTextbox.Text == string.Empty || toDateTextbox.Text == string.Empty)
            {
                messageLabel.Text = "Enter speciific dates";
            }
            else if (!_helper.IsDate(fromDateTextbox.Text) && !_helper.IsDate(toDateTextbox.Text))
            {
                messageLabel.Text = "Valid date fromat is yyyy-mm-dd";
            }
            else
            {
                try
                {
                    TransactionListGridViewBind();
                    btnPrint.Visible = true;
                }
                catch (Exception exception)
                {
                    messageLabel.Text = exception.Message;
                }
            }
        }
        protected void transactionListGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var price = (decimal)DataBinder.Eval(e.Row.DataItem, "TransactionTotal");
                _totalUnitPrice += price;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[2].Text = "Net Total (BDT): ";
                e.Row.Cells[3].Text = _totalUnitPrice.ToString();
            }
        }

        private void TransactionListGridViewBind()
        {
            transactionListGridView.DataSource = _transactionManager.SearchTransactionByDate(Convert.ToDateTime(fromDateTextbox.Text), Convert.ToDateTime(toDateTextbox.Text));
            transactionListGridView.DataBind();

        }

        protected void transactionListGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            transactionListGridView.PageIndex = e.NewPageIndex;
            transactionListGridView.DataBind();
        }

    }
}

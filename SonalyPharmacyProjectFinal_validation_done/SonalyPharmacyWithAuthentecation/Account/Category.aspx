﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.Master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="SonalyPharmacyWithAuthentecation.Account.Category" Async="true"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <div>
        <fieldset>
            <legend class="sidebar-font">Add Category Name</legend>
            <div class="form-group">
                <label class="col-md-2 control-label">Category Name</label>
                <div class="col-md-4">
                    <asp:TextBox CssClass="form-control form-control-width" ID="categoryNameTextBox" runat="server" />
                </div>
            </div>
         
            <hr />
            <div class="form-group">
                <div class="col-md-3 col-md-offset-2">
                    <div class="alert alert-dismissable alert-success" style="margin-top: 1px;">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <asp:Label ID="messageLabel" runat="server">Fill up info and click add!</asp:Label>
                    </div>
                </div>
                <div class="col-md-1">
                    <asp:Button ID="categorySaveButton" runat="server" Text="Add" CssClass="btn btn-warning pull-right" OnClick="categorySaveButton_Click" />
                </div>
            </div>
        </fieldset>
    </div>

</asp:Content>

﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SonalyPharmacyWithAuthentecation.BLL;
using SonalyPharmacyWithAuthentecation.Utility;//1

namespace SonalyPharmacyWithAuthentecation.Account
{
    public partial class Refund : System.Web.UI.Page
    {
        Helper _helper = new Helper();//2
        readonly TransactionManager _transactionManager = new TransactionManager();
        decimal _totalUnitPrice = 0m;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnPrint.Visible = false;
            }
        }

        protected void acceptButton_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void recieptSearchButton_OnClick(object sender, EventArgs e)
        {
            if (reciptIdTextBox.Text == string.Empty)
            {
                messageLabel.Text = "Please insert the receipt ID";
            }
            else
            {
                try
                {
                    TransactionListGridViewBind();
                    ReceiptTextBox.Text = Convert.ToString(reciptIdTextBox.Text);
                    btnPrint.Visible = true;
                    
                }
                catch (Exception exception)
                {
                    messageLabel.Text = exception.Message;
                }
            }
            
        }
        protected void transactionListGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var price = (decimal)DataBinder.Eval(e.Row.DataItem, "TransactionTotal");
                _totalUnitPrice += price;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].Text = "Net Total: ";
                e.Row.Cells[4].Text = _totalUnitPrice.ToString();
            }
        }

        protected void transactionListGridView_OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            transactionListGridView.EditIndex = e.NewEditIndex;
            TransactionListGridViewBind();
        }

        private void TransactionListGridViewBind()
        {
            transactionListGridView.DataSource = _transactionManager.SearchTransaction(Convert.ToString(reciptIdTextBox.Text));
            transactionListGridView.DataBind();
        }

        protected void transactionListGridView_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            
            var transactionId = transactionListGridView.DataKeys[e.RowIndex];
            var transactionIdToInt = Convert.ToInt32(transactionId.Value);
            var previousQuantity = _transactionManager.GetQuantity(transactionIdToInt);
            var recentquantity = ((TextBox) transactionListGridView.Rows[e.RowIndex].FindControl("quantityTextBox")).Text;
            var price = ((Label)transactionListGridView.Rows[e.RowIndex].FindControl("priceLabel")).Text;
            var totalPrice = Convert.ToInt32(recentquantity) * Convert.ToDecimal(price);
            var previousQuantityInt = Convert.ToInt32(previousQuantity);
            var transactionQuantity = Convert.ToInt32(recentquantity);
            var productQuantity = previousQuantityInt - transactionQuantity;

            if (previousQuantity > transactionQuantity)
            {
                transactionListGridView.EditIndex = -1;
                if (transactionId != null)
                    _transactionManager.UpdateTransaction(Convert.ToInt32(transactionId.Value.ToString()), productQuantity,transactionQuantity, totalPrice);
                transactionListGridView.DataSource = _transactionManager.SearchTransaction(reciptIdTextBox.Text);
                transactionListGridView.DataBind();
                
            }
            else
            {
                messageLabel.Text = "Can't be greater than previous quantity";
            }

        }

        protected void transactionListGridView_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            transactionListGridView.EditIndex = -1;
            TransactionListGridViewBind();
        }
    }
}
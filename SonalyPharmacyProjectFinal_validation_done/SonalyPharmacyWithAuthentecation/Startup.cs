﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SonalyPharmacyWithAuthentecation.Startup))]
namespace SonalyPharmacyWithAuthentecation
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SonalyPharmacyWithAuthentecation.DAL.Dao;

namespace SonalyPharmacyWithAuthentecation.DAL.Gateway
{
    public class TransactionGateway : DBGateway
    {
        ProductGateway _productGateway = new ProductGateway();
        public bool SaveTransaction(TransactionModel transaction, List<ProductModel> printedList)
        {
            foreach (var printedListItem in printedList)
            {
                try
                {
                    transaction.TransactionAmount = _productGateway.GetProductPrice(printedListItem.ProductId);
                    ConnectionObj.Open();
                    const string query =
                        "INSERT INTO Table_Transaction(RecieptId,TransactionDate,TransactionQuantity,TransactionTotal,ProductId,CategoryId) VALUES (@RecieptId,@TransactionDate,@TransactionQuantity,@TransactionTotal,@ProductId,@CategoryId)";
                    CommandObj.CommandText = query;
                    CommandObj.Parameters.AddWithValue("@RecieptId", transaction.RecieptId);
                    CommandObj.Parameters.AddWithValue("@TransactionDate", transaction.TransactionDate);
                    CommandObj.Parameters.AddWithValue("@TransactionQuantity", printedListItem.ProductQuantity);
                    CommandObj.Parameters.AddWithValue("@TransactionTotal", printedListItem.ProductQuantity * transaction.TransactionAmount);
                    CommandObj.Parameters.AddWithValue("@ProductId", printedListItem.ProductId);
                    CommandObj.Parameters.AddWithValue("@CategoryId", printedListItem.CategoryId);
                    CommandObj.ExecuteNonQuery();
                    CommandObj.Parameters.Clear();
                }
                catch (Exception exObj)
                {
                    throw exObj;
                }
                finally
                {
                    if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                    {
                        ConnectionObj.Close();
                    }
                }
            }
            return true;
        }

        public DataSet SearchTransaction(string recieptId)
        {
            try
            {
                var query = "SELECT TransactionId, RecieptId, TransactionDate, TransactionQuantity, TransactionTotal, ProductName, SellingPrice, CategoryName  FROM Table_Transaction, Table_Product, Table_Category WHERE Table_Transaction.ProductId = Table_Product.ProductId and Table_Transaction.CategoryId = Table_Category.CategoryId and Table_Transaction.RecieptId='{0}'";
                query = String.Format(query, recieptId);
                CommandObj.CommandText = query;
                var dataAdapterObj = new SqlDataAdapter(query, ConnectionObj);
                var dataSetObj = new DataSet();
                dataAdapterObj.Fill(dataSetObj, "Transaction");
                return dataSetObj;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public bool UpdateTransaction(int transactionId, int productQuantity, int transactionQuantity, decimal totalPrice)
        {
            try
            {
                ConnectionObj.Open();
                const string query = "UPDATE Table_Transaction SET TransactionQuantity=@TransactionQuantity, TransactionTotal=@TransactionTotal WHERE TransactionId=@TransactionId";
                CommandObj.CommandText = query;
                CommandObj.Parameters.AddWithValue("@TransactionQuantity", transactionQuantity);
                CommandObj.Parameters.AddWithValue("@TransactionTotal", totalPrice);
                CommandObj.Parameters.AddWithValue("@TransactionId", transactionId);
                CommandObj.ExecuteNonQuery();
                CommandObj.Parameters.Clear();
                int productId = _productGateway.GetProductId(transactionId);
                _productGateway.UpdateProductQuantity(productId, productQuantity);
                return true;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public int GetQuantity(int transactionId)
        {
            try
            {
                var query = "SELECT * FROM Table_Transaction WHERE TransactionId={0}";
                query = String.Format(query, transactionId);
                CommandObj.CommandText = query;
                ConnectionObj.Open();
                using (SqlDataReader reader = CommandObj.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (int)reader[3];
                    }
                }
                return 0;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public DataSet SearchTransactionByDate(DateTime fromDate, DateTime toDate)
        {
            try
            {
                var query = "SELECT TransactionId, RecieptId, TransactionDate, TransactionQuantity, TransactionTotal, ProductName, SellingPrice, CategoryName  FROM Table_Transaction, Table_Product, Table_Category WHERE Table_Transaction.ProductId = Table_Product.ProductId and Table_Transaction.CategoryId = Table_Category.CategoryId and Table_Transaction.TransactionDate between '{0}' and '{1}'";
                query = String.Format(query, fromDate, toDate);
                CommandObj.CommandText = query;
                var dataAdapterObj = new SqlDataAdapter(query, ConnectionObj);
                var dataSetObj = new DataSet();
                dataAdapterObj.Fill(dataSetObj, "Transaction");
                return dataSetObj;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }
    }
}
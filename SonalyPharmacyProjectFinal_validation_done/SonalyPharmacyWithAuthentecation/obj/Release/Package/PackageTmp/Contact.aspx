﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="SonalyPharmacyWithAuthentecation.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Official Address</h3>
    <address>
        Opposite site of of Jamuna Future Park<br />
        Ka-32,Progati Sharani,Vattara,Dhaka-1229<br />
        <abbr title="Phone">Contact:</abbr>
        +8801752680243
    </address>

</asp:Content>

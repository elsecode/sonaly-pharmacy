﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SonalyPharmacyWithAuthentecation.DAL.Dao;

namespace SonalyPharmacyWithAuthentecation.DAL.Gateway
{
    public class CategoryGateway : DBGateway
    {
        public bool AddCategory(CategoryModel categoryModel)
        {
            try
            {
                ConnectionObj.Open();
                const string query = "INSERT INTO Table_Category(CategoryName) VALUES (@CategoryName)";
                CommandObj.CommandText = query;

                CommandObj.Parameters.AddWithValue("@CategoryName", categoryModel.CategoryName);
               
                CommandObj.ExecuteNonQuery();
                return true;
            }
            catch (Exception exObj)
            {
                throw exObj;
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public DataSet GetCategorySet()
        {
            try
            {
                const string query = "SELECT * FROM Table_Category";
                CommandObj.CommandText = query;
                var dataAdapterObj = new SqlDataAdapter(query, ConnectionObj);
                var dataSetObj = new DataSet();
                dataAdapterObj.Fill(dataSetObj, "Category");
                return dataSetObj;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
        }
    }
}
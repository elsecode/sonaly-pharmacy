﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SonalyPharmacyWithAuthentecation.DAL.Dao;

namespace SonalyPharmacyWithAuthentecation.DAL.Gateway
{
    public class ProductGateway : DBGateway
    {
        public bool AddProduct(ProductModel productModel)
        {
            try
            {
                ConnectionObj.Open();
                const string query = "INSERT INTO Table_Product(ProductName,BuyingPrice,SellingPrice,ProductQuantity,MinimumQuantityRange,ProductPlacement,CategoryId) VALUES (@ProductName,@BuyingPrice,@SellingPrice,@ProductQuantity,@MinimumQuantityRange,@ProductPlacement,@CategoryId)";
                CommandObj.CommandText = query;
                CommandObj.Parameters.AddWithValue("@ProductName", productModel.ProductName);
                CommandObj.Parameters.AddWithValue("@BuyingPrice", productModel.BuyingPrice);
                CommandObj.Parameters.AddWithValue("@SellingPrice", productModel.SellingPrice);
                CommandObj.Parameters.AddWithValue("@ProductQuantity", productModel.ProductQuantity);
                CommandObj.Parameters.AddWithValue("@MinimumQuantityRange", productModel.MinimumQuantityRange);
                CommandObj.Parameters.AddWithValue("@ProductPlacement", productModel.ProductPlacement);
                CommandObj.Parameters.AddWithValue("@CategoryId", productModel.CategoryId);
                CommandObj.ExecuteNonQuery();
                return true;
            }
            catch (Exception exObj)
            {
                throw exObj;
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public DataSet GetProductSet(int selectedCategoryValue)
        {
            try
            {
                var query = "SELECT * FROM Table_Product WHERE CategoryId={0}";
                query = String.Format(query, selectedCategoryValue);
                CommandObj.CommandText = query;
                var dataAdapterObj = new SqlDataAdapter(query, ConnectionObj);
                var dataSetObj = new DataSet();
                dataAdapterObj.Fill(dataSetObj, "Category");
                return dataSetObj;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public bool CheckQuantity(int quantity, int selectedProductValue)
        {
            try
            {
                var query = "SELECT * FROM Table_Product WHERE ProductId={0}";
                query = String.Format(query, selectedProductValue);
                CommandObj.CommandText = query;
                ConnectionObj.Open();
                using (SqlDataReader reader = CommandObj.ExecuteReader())
                {
                    if (!reader.Read()) return false;
                    if ((int)reader[4] >= quantity)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }

        }

        public bool UpdateProduct(List<ProductModel> printedProduct)
        {
            foreach (var productModel in printedProduct)
            {
                int finalQuantity = GetQuantity(productModel.ProductId) - productModel.ProductQuantity;
                try
                {
                    ConnectionObj.Open();
                    const string query = "UPDATE Table_Product SET ProductQuantity=@ProductQuantity WHERE ProductId=@ProductId";
                    CommandObj.CommandText = query;
                    CommandObj.Parameters.AddWithValue("@ProductId", productModel.ProductId);
                    CommandObj.Parameters.AddWithValue("@ProductQuantity", finalQuantity);
                    CommandObj.ExecuteNonQuery();
                    CommandObj.Parameters.Clear();
                }
                catch (Exception exception)
                {
                    throw new ApplicationException(exception.Message);
                }
                finally
                {
                    if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                    {
                        ConnectionObj.Close();
                    }
                }
            }
            return true;
        }

        private int GetQuantity(int productId)
        {
            try
            {
                var query = "SELECT * FROM Table_Product WHERE ProductId={0}";
                query = String.Format(query, productId);
                CommandObj.CommandText = query;
                ConnectionObj.Open();
                using (SqlDataReader reader = CommandObj.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (int)reader[4];
                    }
                }
                return 0;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public decimal GetProductPrice(int selectedProduct)
        {
            try
            {
                var query = "SELECT * FROM Table_Product WHERE ProductId={0}";
                query = String.Format(query, selectedProduct);
                CommandObj.CommandText = query;
                ConnectionObj.Open();
                using (SqlDataReader reader = CommandObj.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (decimal)reader[3];
                    }
                }
                return 0;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public int GetProductId(int transactionId)
        {
            try
            {
                var query = "SELECT  Table_Transaction.ProductId FROM Table_Product, Table_Transaction WHERE Table_Transaction.ProductId=Table_Product.ProductId and Table_Transaction.TransactionId={0}";
                query = String.Format(query, transactionId);
                CommandObj.CommandText = query;
                ConnectionObj.Open();
                using (SqlDataReader reader = CommandObj.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return (int)reader[0];
                    }
                }
                return 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public bool UpdateProductQuantity(int productId, int productQuantity)
        {
            var finalQuantity = GetQuantity(productId) + productQuantity;
            try
            {
                ConnectionObj.Open();
                const string query = "UPDATE Table_Product SET ProductQuantity=@ProductQuantity WHERE ProductId=@ProductId";
                CommandObj.CommandText = query;
                CommandObj.Parameters.AddWithValue("@ProductId", productId);
                CommandObj.Parameters.AddWithValue("@ProductQuantity", finalQuantity);
                CommandObj.ExecuteNonQuery();
                CommandObj.Parameters.Clear();
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }

            return true;
        }

        public bool UpdateProduct(ProductModel product)
        {
            try
            {
                ConnectionObj.Open();
                const string query = "UPDATE Table_Product SET ProductName = @ProductName , BuyingPrice = @BuyingPrice , SellingPrice = @SellingPrice , ProductQuantity = @ProductQuantity , MinimumQuantityRange = @MinimumQuantityRange , ProductPlacement = @ProductPlacement WHERE ProductId = @ProductId";
                CommandObj.CommandText = query;
                CommandObj.Parameters.AddWithValue("@ProductId", product.ProductId);
                CommandObj.Parameters.AddWithValue("@ProductName", product.ProductName);
                CommandObj.Parameters.AddWithValue("@BuyingPrice", product.BuyingPrice);
                CommandObj.Parameters.AddWithValue("@SellingPrice", product.SellingPrice);
                CommandObj.Parameters.AddWithValue("@ProductQuantity", product.ProductQuantity);
                CommandObj.Parameters.AddWithValue("@MinimumQuantityRange", product.MinimumQuantityRange);
                CommandObj.Parameters.AddWithValue("@ProductPlacement", product.ProductPlacement);
                CommandObj.ExecuteNonQuery();
                return true;
            }
            catch (Exception exObj)
            {
                throw exObj;
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public bool DeleteProduct(ProductModel product)
        {
            try
            {
                ConnectionObj.Open();
                const string query = "DELETE FROM Table_Product WHERE ProductId = @ProductId";
                CommandObj.CommandText = query;
                CommandObj.Parameters.AddWithValue("@ProductId", product.ProductId);
                CommandObj.Parameters.AddWithValue("@ProductName", product.ProductName);
                CommandObj.Parameters.AddWithValue("@BuyingPrice", product.BuyingPrice);
                CommandObj.Parameters.AddWithValue("@SellingPrice", product.SellingPrice);
                CommandObj.Parameters.AddWithValue("@ProductQuantity", product.ProductQuantity);
                CommandObj.Parameters.AddWithValue("@MinimumQuantityRange", product.MinimumQuantityRange);
                CommandObj.Parameters.AddWithValue("@ProductPlacement", product.ProductPlacement);
                CommandObj.ExecuteNonQuery();
                return true;
            }
            catch (Exception exObj)
            {
                throw exObj;
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }

        public DataSet GetProductSetByProductId(int selectedProductId)
        {
            try
            {
                var query = "SELECT * FROM Table_Product WHERE ProductId={0}";
                query = String.Format(query, selectedProductId);
                CommandObj.CommandText = query;
                var dataAdapterObj = new SqlDataAdapter(query, ConnectionObj);
                var dataSetObj = new DataSet();
                dataAdapterObj.Fill(dataSetObj, "Product");
                return dataSetObj;
            }
            catch (Exception exception)
            {
                throw new ApplicationException(exception.Message);
            }
            finally
            {
                if (ConnectionObj != null && ConnectionObj.State == ConnectionState.Open)
                {
                    ConnectionObj.Close();
                }
            }
        }
    }
}
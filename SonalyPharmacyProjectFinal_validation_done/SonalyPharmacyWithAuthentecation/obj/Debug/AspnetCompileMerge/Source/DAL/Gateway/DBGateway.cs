﻿using System.Configuration;
using System.Data.SqlClient;

namespace SonalyPharmacyWithAuthentecation.DAL.Gateway
{
    public class DBGateway
    {
        private readonly SqlConnection _connectionObj;
        private readonly SqlCommand _commandObj;

        public DBGateway()
        {
            _connectionObj = new SqlConnection(ConfigurationManager.ConnectionStrings["SonalyConnectionString"].ConnectionString);
            _commandObj = new SqlCommand();
        }

        public SqlConnection ConnectionObj
        {
            get { return _connectionObj; }
        }

        public SqlCommand CommandObj
        {
            get
            {
                _commandObj.Connection = _connectionObj;
                return _commandObj;
            }
        }

        
    }
}
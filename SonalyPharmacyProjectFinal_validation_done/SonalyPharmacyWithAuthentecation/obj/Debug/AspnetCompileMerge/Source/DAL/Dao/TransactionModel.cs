﻿using System;

namespace SonalyPharmacyWithAuthentecation.DAL.Dao
{
    [Serializable]
    public class TransactionModel
    {
        public int TransactionId { get; set; }
        public string RecieptId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal TransactionQuantity { get; set; }
        public decimal TransactionAmount { get; set; }
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
    }
}
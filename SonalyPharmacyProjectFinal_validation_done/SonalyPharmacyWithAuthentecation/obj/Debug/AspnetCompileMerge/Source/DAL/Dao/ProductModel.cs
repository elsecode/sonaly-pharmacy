﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SonalyPharmacyWithAuthentecation.DAL.Dao
{
    [Serializable]
    public class ProductModel
    {
        public int ProductId { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public decimal BuyingPrice { get; set; }
        public decimal SellingPrice { get; set; }
        public int ProductQuantity { get; set; }
        public int MinimumQuantityRange { get; set; }
        public string ProductPlacement { get; set; }
        public int CategoryId { get; set; }
    }
}
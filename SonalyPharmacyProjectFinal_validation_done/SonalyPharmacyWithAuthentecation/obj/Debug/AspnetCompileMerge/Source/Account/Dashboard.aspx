﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="SonalyPharmacyWithAuthentecation.Account.Dashboard" Async="true" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <fieldset>
            <legend class="sidebar-font">Transaction History</legend>
            <div class="container">
                <div class="col-sm-6" style="height: 75px;">
                    <label class="col-md-2 control-label">From</label>
                    <div class='col-md-5'>
                        <div class="form-group">
                            <div class='input-group date' id='datePickerFrom' runat="server">
                                <asp:TextBox type="date" class="form-control" runat="server" ID="fromDateTextbox" />
                            </div>
                        </div>
                    </div>
                    <div class='col-md-5'>
                        <div class="form-group">
                            <label class="col-md-2 control-label">To  </label>
                            <div class='input-group date' id='datePickerTo' runat="server">
                                <asp:TextBox type="date" class="form-control" runat="server" ID="toDateTextbox" />
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $(function () {
                        $('#datePickerFrom').datetimepicker();
                        $('#datePickerTo').datetimepicker();
                        $("#datetimepickerFrom").on("dp.change", function (e) {
                            $('#datetimepickerTo').data("DateTimePicker").setMinDate(e.date);
                        });
                        $("#datetimepickerTo").on("dp.change", function (e) {
                            $('#datetimepickerFrom').data("DateTimePicker").setMaxDate(e.date);
                        });
                        if (!Modernizr.touch || !Modernizr.inputtypes.date) {
                            $('input[type=date]')
                                .attr('type', 'text')
                                .datepicker({
                                    // Consistent format with the HTML5 picker
                                    dateFormat: 'mm-dd-yyyy'
                                });
                        }
                    });
                </script>

            </div>
            <div class="form-group">
                <div class="col-md-3 col-md-offset-1" style="padding-left: 45px;">
                    <div class="alert alert-dismissable alert-success" style="margin-top: 1px; -moz-min-width: 290px; -ms-min-width: 290px; -o-min-width: 290px; -webkit-min-width: 290px; min-width: 290px">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <asp:Label ID="messageLabel" runat="server">Fill up info and click add!</asp:Label>
                    </div>
                </div>
                <div class="col-md-2" style="margin-left: 42px">
                    <asp:Button runat="server" Text="Search" ID="transactionSearchButton" CssClass="btn btn-primary pull-right" OnClick="transactionSearchButton_OnClick" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12" id="divPrint">
                    <div class="form-group hide" id="hideLabel" style="text-align: center">
                        <asp:Label ID="SonalyPharmacyLabel" runat="server" Text="Sonaly Pharmacy" CssClass="alert" Font-Bold="True" Font-Italic="False" Font-Names="Candara" Font-Size="Larger" Font-Strikeout="False" Height="20px"></asp:Label>
                    </div>
                    <div class="form-group hide" id="hideLogo" style="text-align: center">
                        <img border="0" src="/Images/logo.jpg" alt="Pulpit rock" width="75" height="75">
                    </div>

                    <br />
                    <asp:GridView ShowHeaderWhenEmpty="True" CssClass="table table-bordered table-striped" DataKeyNames="TransactionId" ID="transactionListGridView" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="679px" ShowFooter="True" AutoGenerateColumns="False" OnRowDataBound="transactionListGridView_OnRowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Product Type">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="categoryLabel" Text='<%# Eval("CategoryName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="productLabel" Text='<%# Eval("ProductName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Quantity Sold">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="quatityLabel" Text='<%# Eval("TransactionQuantity") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Sell (BDT)">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="totalLabel" Text='<%# Eval("TransactionTotal") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction Date">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="totalLabel" Text='<%# Eval("TransactionDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>

                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />

                    </asp:GridView>
                    <br />
                </div>
            </div>
            <div class="form-group" id="showPrintButton">
                <div class="col-md-4 col-md-offset-5">
                    <input type="button" value="Print " id="btnPrint" runat="Server" onclick="javascript: CallPrint('divPrint')" class="btn btn-primary" />
                </div>
            </div>

        </fieldset>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#transactionSearchButton").click(function () {
                $("#btnPrint").removeClass('btn btn-primary hidden').addClass('btn btn-primary');
            });
        });
        $(document).ready(function () {
            $("#hideLabel").click(function () {
                $("div").show();
            });
            $("#hideLogo").click(function () {
                $("div").show();
            });
        });
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
        $('#btnPrint').click(function () {

            location.reload(true);

        }, 1500);
    </script>

</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.Master" AutoEventWireup="true" CodeBehind="Notification.aspx.cs" Inherits="SonalyPharmacyWithAuthentecation.Account.Notification"  Async="true"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend class="sidebar-font">Notification</legend>
        <div class="form-group">
            <div class="col-md-12" id="divPrint">
                <asp:GridView ShowHeaderWhenEmpty="True" CssClass="table table-bordered" ID="ProductNotificationGridView" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="680px" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="NotificationDataSource" OnRowDataBound="ProductNotificationGridView_RowDataBound" AllowSorting="True">

                    <Columns>
                        <asp:BoundField DataField="ProductName" HeaderText="Product Name" SortExpression="ProductName" />
                        <asp:BoundField DataField="BuyingPrice" HeaderText="Buying Price" SortExpression="BuyingPrice">
                        <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price" SortExpression="SellingPrice">
                        <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductQuantity" HeaderText="Quantity" SortExpression="ProductQuantity">
                        <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinimumQuantityRange" HeaderText="Minimum Quantity Range" SortExpression="MinimumQuantityRange">
                        <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductPlacement" HeaderText="Placement" SortExpression="ProductPlacement">
                        <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>

                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#f9f9f9" Font-Bold="True" ForeColor="Black" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />

                </asp:GridView>
                <asp:SqlDataSource ID="NotificationDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:SonalyConnectionString %>" SelectCommand="SELECT [ProductName], [BuyingPrice], [SellingPrice], [ProductQuantity], [MinimumQuantityRange], [ProductPlacement] FROM [Table_Product] ORDER BY [MinimumQuantityRange] DESC;"></asp:SqlDataSource>
            </div>
        </div>
    </fieldset>
</asp:Content>

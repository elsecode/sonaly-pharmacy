﻿using System;
using System.Data;
using SonalyPharmacyWithAuthentecation.BLL;
using SonalyPharmacyWithAuthentecation.DAL.Dao;
using SonalyPharmacyWithAuthentecation.Utility;//1


namespace SonalyPharmacyWithAuthentecation.Account
{
    public partial class Search : System.Web.UI.Page
    {
        readonly CategoryManager _categoryManager = new CategoryManager();
        ProductManager _productManager = new ProductManager();
        private int _productId;

        Helper _helper = new Helper();//2

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RefreshCategoryDropdownList();
                LoadCategorizedMedicineName(Convert.ToInt32(categoryDropDownList.SelectedValue));
            }
        }

        private void LoadCategorizedMedicineName(int selectedCategoryValue)
        {
            productDropDownList.DataSource = _productManager.GetProductSet(selectedCategoryValue);
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "ProductId";
            productDropDownList.DataBind();
        }

        private void RefreshCategoryDropdownList()
        {
            categoryDropDownList.DataSource = _categoryManager.GetCategorySet();
            categoryDropDownList.DataTextField = "CategoryName";
            categoryDropDownList.DataValueField = "CategoryId";
            categoryDropDownList.DataBind();
        }

        protected void productUpdateButton_OnClick(object sender, EventArgs e)
        {

            if (buyingPriceTextBox.Text == string.Empty 
                || sellingPriceTextBox.Text == string.Empty 
                || productQuantityTextBox.Text == string.Empty
                || minimumQantityRangeTextBox.Text == string.Empty 
                || productPlacementTextBox.Text == string.Empty)
            {
                messageLabel.Text = "Please insert values in all required fields!";

            }
            else
            {
                if (!_helper.IsInteger(productQuantityTextBox.Text))
                {
                    messageLabel.Text = "Product quantity must be a number";
                }

                else if (!_helper.IsInteger(minimumQantityRangeTextBox.Text))
                {
                    messageLabel.Text = "Minimum quantity range must be a number";
                }
                else if (!_helper.IsDecimal(buyingPriceTextBox.Text))
                {
                    messageLabel.Text = "Buying price must be a number";
                }

                else if (!_helper.IsDecimal(sellingPriceTextBox.Text))
                {
                    messageLabel.Text = "Selling price must be a number";
                }

                else
                {
                    try
                    {
                        var product = new ProductModel
                        {
                            ProductId = Convert.ToInt32(productDropDownList.SelectedValue),
                            ProductName = productDropDownList.SelectedItem.ToString(),
                            ProductQuantity = Convert.ToInt32(productQuantityTextBox.Text),
                            SellingPrice = Convert.ToDecimal(sellingPriceTextBox.Text),
                            BuyingPrice = Convert.ToDecimal(buyingPriceTextBox.Text),
                            MinimumQuantityRange = Convert.ToInt32(minimumQantityRangeTextBox.Text),
                            ProductPlacement = productPlacementTextBox.Text
                        };

                        if (_productManager.UpdateProduct(product))
                        {
                            messageLabel.Text = "Product Updated Successfully";
                            productQuantityTextBox.Text = "";
                            sellingPriceTextBox.Text = "";
                            buyingPriceTextBox.Text = "";
                            minimumQantityRangeTextBox.Text = "";
                            productPlacementTextBox.Text = "";

                        }
                        else { messageLabel.Text = "There some error in updating product !!! Please Check everything and Update ."; }
                    }
                    catch (Exception exception)
                    {
                        messageLabel.Text = exception.Message;
                    }

                }
            }
            
        }

        protected void productDeleteButton_OnClick(object sender, EventArgs e)
        {

            if (buyingPriceTextBox.Text == string.Empty && sellingPriceTextBox.Text == string.Empty && productQuantityTextBox.Text == string.Empty && minimumQantityRangeTextBox.Text == string.Empty && productPlacementTextBox.Text == string.Empty)//3
            {
                messageLabel.Text = "Please insert values in all required fields!";

            }
            else//4
            {
                if (!_helper.IsInteger(productQuantityTextBox.Text))//5
                {
                    messageLabel.Text = "Product quantity must be a number";
                }

                else if (!_helper.IsInteger(minimumQantityRangeTextBox.Text))//5
                {
                    messageLabel.Text = "Minimum quantity range must be a number";
                }
                else if (!_helper.IsDecimal(buyingPriceTextBox.Text))//6
                {
                    messageLabel.Text = "Buying price cant be a string";
                }

                else if (!_helper.IsDecimal(sellingPriceTextBox.Text))//6
                {
                    messageLabel.Text = "Selling price cant be a string";
                }

                else
                {
                    try
                    {
                        var product = new ProductModel
                        {
                            ProductId = Convert.ToInt32(productDropDownList.SelectedValue),
                            ProductName = productDropDownList.SelectedItem.ToString(),
                            ProductQuantity = Convert.ToInt32(productQuantityTextBox.Text),
                            SellingPrice = Convert.ToDecimal(sellingPriceTextBox.Text),
                            BuyingPrice = Convert.ToDecimal(buyingPriceTextBox.Text),
                            MinimumQuantityRange = Convert.ToInt32(minimumQantityRangeTextBox.Text),
                            ProductPlacement = productPlacementTextBox.Text
                        };

                        if (_productManager.DeleteProduct(product))
                        {
                            messageLabel.Text = "Product Deleted Successfully";
                            productQuantityTextBox.Text = "";
                            sellingPriceTextBox.Text = "";
                            buyingPriceTextBox.Text = "";
                            minimumQantityRangeTextBox.Text = "";
                            productPlacementTextBox.Text = "";

                        }
                        else { messageLabel.Text = "There some error in deleting product !!! Please Check everything and Delete ."; }
                    }
                    catch (Exception exception)
                    {
                        messageLabel.Text = exception.Message;
                    }

                }
            }
        }

        protected void productSearchButton_OnClick(object sender, EventArgs e)
        {
            DataSet productDataSet = _productManager.GetProductSetByProductId(Convert.ToInt32(productDropDownList.SelectedValue));

            foreach (DataRow row in productDataSet.Tables[0].Rows)
            {
                buyingPriceTextBox.Text = (row["BuyingPrice"].ToString());
                sellingPriceTextBox.Text = (row["SellingPrice"].ToString());
                productQuantityTextBox.Text = (row["ProductQuantity"].ToString());
                minimumQantityRangeTextBox.Text = (row["MinimumQuantityRange"].ToString());
                productPlacementTextBox.Text = (row["ProductPlacement"].ToString());
            }
        }

        protected void categoryDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadCategorizedMedicineName(Convert.ToInt32(categoryDropDownList.SelectedValue));
        }
    }
}
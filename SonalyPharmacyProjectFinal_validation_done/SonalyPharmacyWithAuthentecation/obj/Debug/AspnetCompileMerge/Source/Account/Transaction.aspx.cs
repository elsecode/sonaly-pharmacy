﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using SonalyPharmacyWithAuthentecation.BLL;
using SonalyPharmacyWithAuthentecation.DAL.Dao;
using SonalyPharmacyWithAuthentecation.Utility;//1

namespace SonalyPharmacyWithAuthentecation.Account
{
    public partial class Transaction : System.Web.UI.Page
    {
        readonly CategoryManager _categoryManager = new CategoryManager();
        readonly ProductManager _productManager = new ProductManager();
        readonly TransactionManager _transactionManager = new TransactionManager();
        private DataTable _printDataTable = null;
        private List<ProductModel> _printedList = null;
        decimal _totalUnitPrice = 0m;
        private int _transactionId;

        Helper _helper = new Helper();//2

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RefreshCategoryDropdownList();
                LoadCategorizedMedicineName(Convert.ToInt32(categoryDropDownList.SelectedValue));
                btnPrint.Visible = false;
                acceptButton.Visible = false;
            }
            _printedList = ViewState["_printedList"] as List<ProductModel>;
            if (_printedList == null)
            {
                _printedList = new List<ProductModel>();
            }
            _printDataTable = ViewState["_printDataTable"] as DataTable;
            if (_printDataTable == null)
            {
                _printDataTable = new DataTable();
            }
        }

        private void LoadCategorizedMedicineName(int selectedCategoryValue)
        {
            productDropDownList.DataSource = _productManager.GetProductSet(selectedCategoryValue);
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "ProductId";
            productDropDownList.DataBind();
        }

        private void RefreshCategoryDropdownList()
        {
            categoryDropDownList.DataSource = _categoryManager.GetCategorySet();
            categoryDropDownList.DataTextField = "CategoryName";
            categoryDropDownList.DataValueField = "CategoryId";
            categoryDropDownList.DataBind();
        }

        private void RefreshProductDropdownList()
        {
            productDropDownList.DataSource = _productManager.GetProductSet(Convert.ToInt32(categoryDropDownList.SelectedValue));
            productDropDownList.DataTextField = "ProductName";
            productDropDownList.DataValueField = "productId";
            productDropDownList.DataBind();
        }

        protected void addProductToGrid_Click(object sender, EventArgs e)
        {
            if(productQuantityTextBox.Text==string.Empty)//3
            {
                messageLabel.Text = "Please insert quantity!";

            }
            else//4
            {
                if (!_helper.IsInteger(productQuantityTextBox.Text))//5
                {
                    messageLabel.Text = "Product quantity must be a number";
                }
                else
                {
                    try//6 start
                    {
                        if (!_productManager.CheckQuantity(Convert.ToInt32(productQuantityTextBox.Text), Convert.ToInt32(productDropDownList.SelectedValue)))
                        {
                            messageLabel.Text = "Not enough medicine available!";
                        }
                        else
                        {
                            var product = new ProductModel()
                            {
                                ProductId = Convert.ToInt32(productDropDownList.SelectedValue),
                                ProductQuantity = Convert.ToInt32(productQuantityTextBox.Text),
                                CategoryId = Convert.ToInt32(categoryDropDownList.SelectedValue)
                            };

                            if (_printDataTable.Columns.Count == 0)
                            {
                                _printDataTable.Columns.Add("Product Type", typeof(string));
                                _printDataTable.Columns.Add("Product Name", typeof(string));
                                _printDataTable.Columns.Add("Quantity", typeof(int));
                                _printDataTable.Columns.Add("Price (BDT)", typeof(decimal));
                                _printDataTable.Columns.Add("Total", typeof(decimal));
                            }

                            decimal productPrice = _productManager.GetProductPrice(Convert.ToInt32(productDropDownList.SelectedValue));
                            DataRow row = _printDataTable.NewRow();
                            row[0] = categoryDropDownList.SelectedItem;
                            row[1] = productDropDownList.SelectedItem;
                            row[2] = productQuantityTextBox.Text;
                            row[3] = _productManager.GetProductPrice(Convert.ToInt32(productDropDownList.SelectedValue));
                            row[4] = productPrice*Convert.ToInt32(productQuantityTextBox.Text); 
                            _printDataTable.Rows.Add(row);
                
                             ViewState["_printDataTable"] = _printDataTable;
                
                            ProductRecieptGridView.DataSource = _printDataTable;
                            ProductRecieptGridView.DataBind();

                            _printedList.Add(product);
                            ViewState["_printedList"] = _printedList;//6
                            acceptButton.Visible = true;
                            
                       }

                    }



                catch (Exception exception)//7
                {
                    messageLabel.Text = exception.Message;
                }
            }
            }

       }
        

        protected void printGridView_Click(object sender, EventArgs e)
        {

        }

        protected void acceptButton_Click(object sender, EventArgs e)
        {
            if (_productManager.UpdateProduct(_printedList))
            {
                messageLabel.Text = "Transaction Completed!";
                var transaction = new TransactionModel {TransactionId = _transactionId};
                var random = new Random();
                for (var i = 0; i < 8; i++)
                {
                    transaction.RecieptId += random.Next(10).ToString();
                }
                transaction.TransactionDate = DateTime.Now.Date;
                ReceiptTextBox.Text = transaction.RecieptId;
                _transactionManager.SaveTransaction(transaction, _printedList);
                btnPrint.Visible = true;

            }

        }

        protected void ProductRecieptGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
            e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right; 
            e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
            e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
            e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var price = (decimal)DataBinder.Eval(e.Row.DataItem, "Total");
                _totalUnitPrice += price;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].Text = "Net Total (BDT): ";
                e.Row.Cells[4].Text = _totalUnitPrice.ToString();
            }
        }

        protected void categoryDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshProductDropdownList();
        }
    }
}
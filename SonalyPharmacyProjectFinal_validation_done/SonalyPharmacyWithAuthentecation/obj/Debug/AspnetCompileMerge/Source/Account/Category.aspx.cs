﻿using System;
using SonalyPharmacyWithAuthentecation.BLL;
using SonalyPharmacyWithAuthentecation.DAL.Dao;
using SonalyPharmacyWithAuthentecation.Utility;//1

namespace SonalyPharmacyWithAuthentecation.Account
{
    public partial class Category : System.Web.UI.Page
    {
        Helper _helper = new Helper();//2
        private int _categoryId;
        CategoryManager _categoryManager = new CategoryManager();

        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void categorySaveButton_Click(object sender, EventArgs e)
        {
            if (categoryNameTextBox.Text == string.Empty)
            {
                messageLabel.Text = "Please insert category name!";
            }

            else
            {
                try
                {
                    var category = new CategoryModel
                    {

                        CategoryId = _categoryId,
                        CategoryName = categoryNameTextBox.Text

                    };

                    messageLabel.Text = _categoryManager.AddCategory(category) ? "Category Added Successfully" : "There some error in adding category";
                    categoryNameTextBox.Text = "";
                }
                catch (Exception exception)
                {
                    messageLabel.Text = exception.Message;
                }
            }
        }

    }
}
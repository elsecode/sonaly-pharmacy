﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SonalyPharmacyWithAuthentecation.Account
{
    public partial class Notification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ProductNotificationGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var minimumQuantityRange = (int)DataBinder.Eval(e.Row.DataItem, "MinimumQuantityRange");
                var productQuantity = (int)DataBinder.Eval(e.Row.DataItem, "ProductQuantity");

                if (productQuantity <= minimumQuantityRange)
                {
                    e.Row.BackColor = Color.Orange;
                }
            }
        }
    }
}
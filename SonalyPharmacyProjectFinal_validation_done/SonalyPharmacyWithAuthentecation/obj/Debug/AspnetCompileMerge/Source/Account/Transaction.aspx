﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard.Master" AutoEventWireup="true" CodeBehind="Transaction.aspx.cs" Inherits="SonalyPharmacyWithAuthentecation.Account.Transaction" Async="true" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <fieldset>
            <legend class="sidebar-font">Transaction</legend>
            <div class="form-group">
                <label class="col-md-2 control-label">Product Category</label>
                <div class="col-md-4">
                    <asp:DropDownList CssClass="form-control" ID="categoryDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="categoryDropDownList_SelectedIndexChanged" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Product Name</label>
                <div class="col-md-4">
                    <asp:DropDownList CssClass="form-control" ID="productDropDownList" runat="server" AutoPostBack="True" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Product Quantity</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:TextBox ID="productQuantityTextBox" runat="server" CssClass="form-control" />
                        <span class="input-group-addon">Packet/Piece</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 col-md-offset-2">
                    <div class="alert alert-dismissable alert-success" style="margin-top: 1px;">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <asp:Label ID="messageLabel" runat="server">Fill up info and click add!</asp:Label>
                    </div>
                </div>
                <div class="col-md-1">
                    <asp:Button ID="addProductToGrid" runat="server" Text="Add" CssClass="btn btn-warning pull-right" OnClick="addProductToGrid_Click" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" id="divPrint">
                    <div class="form-group hide" id="hideLabel" style="text-align: center">
                        <asp:Label ID="SonalyPharmacyLabel" runat="server" Text="Sonaly Pharmacy" CssClass="alert" Font-Bold="True" Font-Italic="False" Font-Names="Candara" Font-Size="Larger" Font-Strikeout="False" Height="20px"></asp:Label>
                    </div>
                    <div class="form-group hide" id="hideLogo" style="text-align: center">
                        <img border="0" src="/Images/logo.jpg" alt="Pulpit rock" width="75" height="75">
                    </div>
                    <div class="form-group" style="text-align: center">
                        <div class="col-md-4 hide" id="hideReceipt" style="text-align: center">
                            <asp:Label ID="ReceiptLabel" runat="server" Text="Receipt Number:"></asp:Label>
                            <asp:TextBox ID="ReceiptTextBox" runat="server" AutoPostBack="True" ReadOnly="True"></asp:TextBox>
                        </div>
                    </div><br/> 

                    <asp:GridView ShowHeaderWhenEmpty="True" CssClass="table table-bordered table-striped" ID="ProductRecieptGridView" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="680px" OnRowDataBound="ProductRecieptGridView_RowDataBound" ShowFooter="True">

                        <FooterStyle BackColor="White" ForeColor="#000066" />

                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />

                    </asp:GridView><br/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 col-md-offset-3">
                    <asp:Button runat="server" CssClass="btn btn-primary" Text="Accept" ID="acceptButton" OnClick="acceptButton_Click" />
                    <input type="button" value="Print " id="btnPrint" runat="Server" onclick="javascript: CallPrint('divPrint')" class="btn btn-primary pull-right" />
                </div>
            </div>
        </fieldset>
    </div>
    <script type="text/javascript">
        $("#hideLabel").click(function(){
                $("div").show();
                });
        $("#hideLogo").click(function(){
                $("div").show();
                });
        $("#hideReceipt").click(function(){
                $("div").show();
                });
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=400,height=400,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.write('Thank you for your purchasing . Take care of yourself.');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
        $('#btnPrint').click(function () {

            location.reload(true);

        },1500);
    </script>
</asp:Content>
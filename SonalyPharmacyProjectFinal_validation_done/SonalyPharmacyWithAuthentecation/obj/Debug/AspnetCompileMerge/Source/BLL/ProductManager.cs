﻿using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using SonalyPharmacyWithAuthentecation.DAL.Dao;
using SonalyPharmacyWithAuthentecation.DAL.Gateway;

namespace SonalyPharmacyWithAuthentecation.BLL
{
    public class ProductManager
    {
        readonly ProductGateway _productGateway = new ProductGateway();
        public bool AddProduct(ProductModel productModel)
        {
            return _productGateway.AddProduct(productModel);
        }

        public DataSet GetProductSet(int selectedCategoryValue)
        {
            return _productGateway.GetProductSet(selectedCategoryValue);
        }

        public bool CheckQuantity(int quantity,int selectedProductValue)
        {
            return _productGateway.CheckQuantity(quantity, selectedProductValue);
        }

        public bool UpdateProduct(List<ProductModel> printedProduct)
        {
            return _productGateway.UpdateProduct(printedProduct);
        }

        public decimal GetProductPrice(int selectedProduct)
        {
            return _productGateway.GetProductPrice(selectedProduct);
        }
        public bool DeleteProduct(ProductModel product)
        {
            return _productGateway.DeleteProduct(product);
        }

        public bool UpdateProduct(ProductModel product)
        {
            return _productGateway.UpdateProduct(product);
        }

        public DataSet GetProductSetByProductId(int selectedProductId)
        {
            return _productGateway.GetProductSetByProductId(selectedProductId);
        }
    }
}
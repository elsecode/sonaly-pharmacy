﻿using System.Data;
using SonalyPharmacyWithAuthentecation.DAL.Dao;
using SonalyPharmacyWithAuthentecation.DAL.Gateway;

namespace SonalyPharmacyWithAuthentecation.BLL
{
    public class CategoryManager
    {
        readonly CategoryGateway _categoryGateway = new CategoryGateway();

        public bool AddCategory(CategoryModel categoryModel)
        {
            return _categoryGateway.AddCategory(categoryModel);
        }

        public DataSet GetCategorySet()
        {
            return _categoryGateway.GetCategorySet();
        }
    }
}
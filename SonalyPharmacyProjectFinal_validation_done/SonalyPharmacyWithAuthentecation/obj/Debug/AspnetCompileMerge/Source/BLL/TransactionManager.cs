﻿using System;
using System.Collections.Generic;
using System.Data;
using SonalyPharmacyWithAuthentecation.DAL.Dao;
using SonalyPharmacyWithAuthentecation.DAL.Gateway;
using SonalyPharmacyWithAuthentecation.Utility;

namespace SonalyPharmacyWithAuthentecation.BLL
{
    public class TransactionManager
    {
        TransactionGateway _transactionGateway = new TransactionGateway();
        Helper _helper = new Helper();
        public void SaveTransaction(TransactionModel transaction, List<ProductModel> printedList)
        {
            _transactionGateway.SaveTransaction(transaction, printedList);
        }

        public DataSet SearchTransaction(string recieptId)
        {
            return _transactionGateway.SearchTransaction(recieptId);
        }

        public bool UpdateTransaction(int transactionId, int productQuantity, int transactionQuantity, decimal totalPrice)
        {
            return _transactionGateway.UpdateTransaction(transactionId, productQuantity, transactionQuantity, totalPrice);
        }

        public int GetQuantity(int transactionId)
        {
            return _transactionGateway.GetQuantity(transactionId);
        }

        public DataSet SearchTransactionByDate(DateTime fromDate, DateTime toDate)
        {
            return _transactionGateway.SearchTransactionByDate(fromDate, toDate);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SonalyPharmacyWithAuthentecation.Utility
{
    public class Helper
    {
        public bool IsDecimal(string value)
        {
            decimal number;
            return decimal.TryParse(value,out number);
        }
        public bool IsInteger(string value)
        {
            int number;
            return Int32.TryParse(value, out number);
        }

        public bool IsDate(string value)
        {
            DateTime date;
            return DateTime.TryParse(value, out date);
        }
    }
}